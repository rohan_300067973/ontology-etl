import pandas as pd
import json
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


def write_to_csv(row_list, filename):
    df = pd.DataFrame(row_list)
    df.to_csv(filename)

def get_list_from_json(response, row_list):
    row = dict()
    ct = 0

    for every_key in response:
        row = {}
        print(every_key)
        category = ""
        description = ""
        title = ""
        color = ""
        gender = ""
        brand = ""
        path = ""
        skuId = ""
        mrp = 0
        if response[every_key]["catalogEag"] != None:
            json_data = response[every_key]["catalogEag"]
            if "category" in json_data:
                category = json_data["category"]
            if "description" in json_data:
                description = json_data["description"]
            if "title" in json_data:
                title = json_data["title"]
            if "color" in json_data:
                color = json_data["color"]
            if "gender" in json_data:
                gender = json_data["gender"]
            if "brand" in json_data:
                brand = json_data["brand"]
            if "breadCrumb" in json_data:
                breadCrumb = json_data["breadCrumb"]
            for each_element in breadCrumb:
                path = path + each_element.lower().replace("_", "") + " "
            if skuId in response[every_key]:
                skuId = response[every_key]["skuId"]
            if "mrp" in response[every_key]["listingEags"][0]:
                mrp = response[every_key]["listingEags"][0]["mrp"]

            row["style_id"] = skuId
            row["articletype"] = category.lower().replace(" ", "_")
            row["brand"] = brand.lower().replace("_", "")
            row["pdpdata"] = description.lower() + title.lower().replace("_", "") + color.lower().replace("_", "") + gender.lower() + path
            row["brand_articletype"] = row["brand"] + ":-:" + row["articletype"]

            #print(row)
            row_list.append(row)
    return row_list

def get_data_from_api():
    row_list = list()

    with open('/Users/300067973/Documents/ontology/repos/ontology-etl/data/deep_crawl_data_flipkart.json', 'r') as f:
        response = json.load(f)
    row_list = get_list_from_json(response, row_list)
    print(row_list)
    write_to_csv(row_list, "/Users/300067973/Documents/ontology/repos/ontology-etl/data/flipkart_all_style_data.csv")

if __name__=="__main__":
    get_data_from_api()