import pandas as pd
import requests
import time
import json
import pprint
import os, sys


def write_to_csv(row_list, filename):
    df = pd.DataFrame(row_list)
    df.to_csv(filename)

def format(word):
    return word.lower().replace(" ", "_")

def get_list_from_json(response, row_list):
    row_process = dict()
    ct = 0

    for each in response:
        try:
            row = {}
            ct = ct + 1
            key = response[each]["productId"]
            row["productId"] = key
            category = response[each]["catalogEag"]["category"]
            row["category"] = format(category)
            description = response[each]["catalogEag"]["description"]
            row["description"] = description.lower()
            title = response[each]["catalogEag"]["title"]
            row["title"] = title.lower()
            gender = response[each]["catalogEag"]["gender"]
            row["gender"] = format(gender)
            brand = response[each]["catalogEag"]["brand"]
            row["brand"] = format(brand)
            path = ""
            breadCrumb = response[each]["catalogEag"]["breadCrumb"]
            for each1 in breadCrumb:
                path = path + " " + each1
            breadCrumb = path
            row["breadCrumb"] = path.lower()
            print(row)
            row_list.append(row)
        except:
            continue

    print(ct)
    return row_list


def get_data_from_api():
    row_list = list()

    with open('/data/deep_crawl_data_nykaa.json', 'r') as f:
        response = json.load(f)
    row_list = get_list_from_json(response, row_list)
    print(len(row_list))
    write_to_csv(row_list, "/Users/300067973/Documents/ontology/repos/ontology-etl/data/nykaa_all_style_data.csv")


if __name__ == "__main__":
    get_data_from_api()