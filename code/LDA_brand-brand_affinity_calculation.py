import pickle
import pandas as pd
import re
import os
import numpy as np

import gensim
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.feature_extraction.text import CountVectorizer
import sklearn.metrics.pairwise as tm

from gensim.models import Doc2Vec
from re import search
import numpy as np
import csv
import warnings
from sklearn.decomposition import LatentDirichletAllocation as LDA

from collections import Counter

warnings.simplefilter("ignore", DeprecationWarning)


def load_dataframe(path):

    df = pd.read_csv(path)
    df = df.drop(['style_id'], axis=1)
    #df = df.drop(['style_id'], axis=1)
    df = df.rename(columns={"Unnamed: 0":"style_id"})
    for i in df.columns:
        print(i)
    df.drop(df.filter(regex="Unname"),axis=1, inplace=True)
    print("datafreame loaded.")
    return df


def translate(match):
    word = match.group(0).lower()
    return replace_dict.get(word, word)

def word_replace(text, replace_dict):
    rc = re.compile(r"[A-Za-z_]\w*")
    return rc.sub(translate, text)


def remove_common_words(df):
    print(Counter(" ".join(df["pdpdata"]).split()).most_common(10))


    replace_dict = {
        "values" : '',
        "type" : '',
        "subheading" : '',
        "category" : '',
        "data" : '',
        "map" : '',
        "attributes" : '',
        "config" : '',
        "sku" : '',
        "size" : '',
        "name" : '',
        "heading" : '',
        "material" : ''

    }                            # {"words_to_find" : 'word_to_replace'}

    l3 = ["values",
          "type",
          "subheading",
          "category",
          "data",
          "map",
          "attributes",
          "config",
          "sku",
          "size",
          "name",
          "heading",
          "material"
          ]

    df = df.replace(l3, '')
    return df


def phrase_embedding(df):
    count_vectorizer = CountVectorizer(stop_words='english')
    # Fit and transform the processed titles
    count_data = count_vectorizer.fit_transform(df['pdpdata'])
    print("count vectorise completed.")
    return count_data, count_vectorizer




def print_topics(model, count_vectorizer, n_top_words):
    words = count_vectorizer.get_feature_names()
    for topic_idx, topic in enumerate(model.components_):
        print("\nTopic #%d:" % topic_idx)
        print(" ".join([words[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))

def topic_creation(number_topics, count_vectorizer, number_words):

    # Create and fit the LDA model
    print("model started")
    lda = LDA(n_components=number_topics, n_jobs=-1)
    print("first step completed.")
    lda.fit(count_data)
    print("second step completed.")
    ladtopicmatrix = lda.transform(count_data)
    # Print the topics found by the LDA model
    print("Topics found via LDA:")
    print_topics(lda, count_vectorizer, number_words)
    return ladtopicmatrix



def aggrgate_brand_articletype(df):

    df1 = df.groupby('brand_articletype')['style_id'].apply(list).reset_index(name='new')
    final_list = list()
    for i in range(0, df1.shape[0]):
        temp = list()
        temp.append(df1['brand_articletype'][i])
        temp.append(df1['new'][i])
        final_list.append(temp)

    print("Converted df1 to final_list")
    print("final_list length: ", str(len(final_list)))
    print(final_list)
    return final_list



def affinity_calculation(ldatopicmatrix, final_list):
    with open('affinities_final_cp.csv', 'w') as csvFile:
        writer = csv.writer(csvFile)
        bat1 = ""
        for i in range(0, len(final_list)):
            for j in range(i+1, len(final_list)):
                bat1 = final_list[i][0].split(":-:")
                print(bat1)
                at1 = bat1[1]
                bat2 = final_list[j][0].split(":-:")
                print(bat2)
                at2 = bat2[1]
                if at1 != at2:
                    continue

                affinity = 0
                ct = 0
                for k in final_list[i][1]:
                    for m in final_list[j][1]:
                        ct += 1
                        affinity_from_cosine = tm.cosine_similarity(ldatopicmatrix[k].reshape(len(ldatopicmatrix[k]), 1).T, ldatopicmatrix[m].reshape(len(ldatopicmatrix[m]), 1).T)
                        affinity += affinity_from_cosine[0][0]
                templ = list()
                print("i=", i, ", j=", j)
                print("Affinity sum=", affinity)
                if(ct != 0):
                    templ.append(final_list[i][0])
                    templ.append(final_list[j][0])
                    affinity_avg = affinity/ct
                    templ.append(affinity_avg)
                    print("Affinity average= ", affinity_avg)
                    writer.writerow(templ)
    print("----End of script----")
    csvFile.close()




if __name__== "__main__":

    path = '/Users/300067973/Documents/ontology/repos/ontology-etl/data/flipkart_all_style_data.csv'

    # loading dataframe
    df = load_dataframe(path)
    print(df)
    df = remove_common_words(df)
    count_data, count_vectorizer = phrase_embedding(df)
    number_topics = 5
    number_words = 10
    ldatopicmatrix = topic_creation(number_topics, count_vectorizer, number_words)
    print(ldatopicmatrix[0])
    final_list = aggrgate_brand_articletype(df)
    print(final_list)
    affinity_calculation(ldatopicmatrix, final_list)
