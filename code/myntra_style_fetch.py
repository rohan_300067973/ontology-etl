import pandas as pd
import requests
import time

service_url = "http://catalogservice.myntra.com/myntra-catalog-service/"
api_headers = {'Authorization': 'Basic ZGVlcGlrYSBnaG9ka2l+ZGVlcGlrYS5naG9ka2k6IiI=', 'Accept': 'application/json', 'Content-Type': 'application/json'}


def write_to_csv(row_list, filename):
    df = pd.DataFrame(row_list)
    df.to_csv(filename)

def format(word):
    return word.strip().lower().replace(" ", "_")


def get_list_from_json(response, row_list):
    row = dict()
    for each in response['data']:

            row = {}
            row["productId"] = each["productId"]
            print(each["productId"])
            row["title"] = each["title"].lower().strip()
            row["price"] = each["price"]
            row["productDisplayName"] = each["productDisplayName"].lower().strip()
            row["articleType"] = format(each["articleType"]["typeName"])
            row["subCategory"] = format(each["subCatagory"]["typeName"])
            row["masterCategory"] = format(each["masterCatagory"]["typeName"])
            jarray = each["productArticleTypeAttributeValues"]
            pdpdata = ""
            for each1 in jarray:
                pdpdata += format(each1["attributeType"]["typeName"]) + " "
                pdpdata += format(each1["attributeValue"]) + " "
            row["atsa"] = pdpdata
            jarray = each["globalAttributes"]
            for each1 in jarray:
                if "attributeValue" in each["globalAttributes"][each1]:
                    row[each1] = format(each["globalAttributes"][each1]["attributeValue"])
                else:
                   row[each1] = None
            row_list.append(row)
    return row_list


def get_data_from_api():
    row_list = list()
    url = service_url + "v2/product/search/paging?q=styleType.eq:P&sortBy=productId&sortOrder=DESC&fetchSize=1000"
    response = requests.get(url,  headers=api_headers).json()
    row_list = get_list_from_json(response, row_list)
    next_cursor_marker = response["nextCursorMark"]
    print(next_cursor_marker)
    old_cursor = ""

    while True:
        if next_cursor_marker == old_cursor:
            break
        url = service_url + "v2/product/search/paging?q=styleType.eq:P&sortBy=productId&sortOrder=DESC&fetchSize=100&cursorMark="+next_cursor_marker
        t1 = time.time()
        response = requests.get(url,  headers=api_headers).json()
        print(time.time() - t1)
        row_list = get_list_from_json(response, row_list)
        old_cursor = next_cursor_marker
        next_cursor_marker = response["nextCursorMark"]
        next_cursor_marker = next_cursor_marker.replace("+", "%2B")
        print(next_cursor_marker)

    write_to_csv(row_list, "../data/myntra_all_style_data.csv")

if __name__=="__main__":
    get_data_from_api()
