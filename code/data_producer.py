import logging

import pandas as pd
import json
import time
import yaml

from producer import airbus_producer
from entry import event_entry


def on_success(msg):
    logger.info('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))


def on_failure(err, msg):
    logger.error('Message delivery failed: {}'.format(err))


def ingest_data(path, airbusProducer):
    myntra_data = pd.read_csv(path)
    print(myntra_data)
    source = "myntra"
    headers = list(myntra_data)
    for ind in myntra_data.index:
        json_data = {}
        json_data["type"] = "vertex"
        data = {}
        data1 = {}
        data1["nodeLabel"] = "brand"
        data1["name"] = myntra_data["brandName"][ind]
        data1["code"] = "brand" + ":-:" + data1["name"]
        data1["sources"] = source
        data1["isSourceMyntra"] = "true"
        data1["createdOn"] = time.time()
        data1["createdBy"] = "cms_ontology_engg@myntra.com"
        data1["lastModifiedOn"] = time.time()
        data1["lastModifiedBy"] = "cms_ontology_engg@myntra.com"
        json_property = json.dumps(data1)
        data["properties"] = json_property
        json_data["data"] = json.dumps(data)
        final_json = json.dumps(json_data)
        event = event_entry.EventEntry(app_name=cfg['app_name'], event_name=cfg['event_name'], data=final_json)
        airbusProducer.send_message(event)
        print(final_json)


if __name__ == '__main__':
    logger = logging.getLogger('spam_application')
    logger.setLevel(logging.DEBUG)

    myntra_path = "/Users/300067973/Documents/ontology/repos/ontology-etl/data/myntra_all_style_data.csv"



    with open("/Users/300067973/Documents/ontology/repos/ontology-etl/config/airbus-config.yaml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)

    airbusProducer = airbus_producer.AirbusProducer(app_name=cfg['app_name'],
                                                    service_url=cfg['service_url'],
                                                    on_success_callback=on_success,
                                                    on_failure_callback=on_failure)

    ingest_data(myntra_path, airbusProducer)
    airbusProducer.shutdown_producer()